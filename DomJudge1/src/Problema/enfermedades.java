package Problema;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class enfermedades {
	public static void main(String[] args) {

		init();
	}

	public static void init() {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		int casos;
		casos = scanner.nextInt();
		for (int i = 0; i < casos; i++) {
			int enfermedades = scanner.nextInt();
			ArrayList<Integer> lista = new ArrayList<Integer>();
			for (int j = 0; j < enfermedades; j++) {
				int infectados = scanner.nextInt();
				int fallecidos = scanner.nextInt();
				int resultado = (fallecidos * 100) / infectados;
				lista.add(resultado);
				scanner.nextLine();
			}
			System.out.println(max(lista));
		}
	}

	static String max(ArrayList<Integer> lista) {
		// TODO Auto-generated method stub
		int max = Collections.max(lista);
		int pos = 0;
		for (int i = 0; i < lista.size(); i++) {
			if (lista.get(i) == max) {
				pos = lista.indexOf(max) + 1;
			}
		}
		if (max <= 100) {
			return max + "%" + " " +pos;
		} else {
			return "Impossible";
		}
	}
}