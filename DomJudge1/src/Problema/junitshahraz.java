package Problema;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Test;

class junitshahraz {

	@Test
	void testosBasics() {
		ArrayList<Integer> lista = new ArrayList<Integer>(Arrays.asList(20,30,40,50,60));
		ArrayList<Integer> lista1 = new ArrayList<Integer>(Arrays.asList(80,90,56,96,34));
		ArrayList<Integer> lista2 = new ArrayList<Integer>(Arrays.asList(150,30,40,50,60));
		ArrayList<Integer> lista3 = new ArrayList<Integer>(Arrays.asList(45,60,80,90,-5));
		ArrayList<Integer> lista4 = new ArrayList<Integer>(Arrays.asList(48,99,22,30,100));

		assertEquals("60% 5", enfermedades.max(lista));
		assertEquals("96% 4", enfermedades.max(lista1));
		assertEquals("Impossible", enfermedades.max(lista2));
		assertEquals("90% 4", enfermedades.max(lista3));
		assertEquals("100% 5", enfermedades.max(lista4));

	}

}